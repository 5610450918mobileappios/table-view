//
//  prettyViewController.swift
//  table
//
//  Created by Teerapat on 9/22/2558 BE.
//  Copyright (c) 2558 Teerapat. All rights reserved.
//

import UIKit

class prettyViewController: UIViewController,UITableViewDataSource, UITableViewDelegate{
    var foodname = ["Stake","Bread","Apple","Banana"]
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        
        return self.foodname.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("boss", forIndexPath: indexPath) as! TableViewCell
        
        // Configure the cell...
        
        //        cell.textLabel?.text = self.foodname[indexPath.item]
        cell.foodName.text = foodname[indexPath.item]
        return cell
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
