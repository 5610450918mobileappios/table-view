//
//  ViewController.swift
//  table
//
//  Created by Teerapat on 9/22/2558 BE.
//  Copyright (c) 2558 Teerapat. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var foodname:String!
    var foodDetail:String!
    
    @IBOutlet weak var detailLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
       
        detailLabel.text = self.foodDetail
        self.navigationItem.title = self.foodname
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

